﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity; 

namespace eCommerce.Models
{
    public class ProductDataContext: DbContext
    {
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductImage> ProductImages { get; set; }

        public ProductDataContext()
            : base("DefaultConnection")
        {
        }
    }
}